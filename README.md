## Description
An open source weather station using a Raspberry Pi and DHT-22 sensor as data source. The data is presented in an R Shiny dashboard. See demo at: [http://weather-station.hanshendrikhuber.de](http://weather-station.hanshendrikhuber.de)

![figure1](docs/img/structure.png "Project structure")


## What you need
- DHT-22 Sensor (for measuring temperature and humidity data)
- Raspberry Pi (tested on model 1B)
- Webspace (for storing the data)
- shinyapps.io account (free plan - for hosting the dashboard)
- (Sub-) domain (optional - to access the weather station under your domain)


## Setup
### Setting up the sensor: Hardware
Connect the sensor to the pi like in the figure below.

![figure2](docs/img/connection-rpi-dht22.png "Wiring diagram")

### Setting up the sensor: Software
In this section we set up all the software we need on the pi to log data with the sensor.

1. First update your package list by running: `sudo apt-get update`
2. Install all necessary build tools we need further on by running: `sudo apt-get install build-essential python-dev python-openssl git`
3. Download Adafruit Python DHT22 drivers on your pi by running: `git clone https://github.com/adafruit/Adafruit_Python_DHT.git`
4. Move into the downloaded directory and install the drivers by running: `cd Adafruit_Python_DHT && sudo python setup.py install`

Now we are done with all the basics and the sensor should be set up and ready to go. At this point you can check if everything works by running `python AdafruitDHT.py 22 4` from inside the examples folder in Adafruit\_Python\_DHT. If you get a error message `Failed to get reading. Try again!` don't worry. The DHT-22 seems to be pretty unreliable and a failed reading happens from time to time.

### Setting up the dashboard

1. Download this repository to your pi by running `git clone https://gitlab.com/chillwalker/weather-station.git`
2. Schedule a cronjob to save temperature and humidity by executing `crontab -e` and adding ```* * * * * /usr/bin/env python <ABSOLUTE-PATH-TO-REPOSITORY>/get-and-save.py```
3. Schedule another cronjob to upload the data every 15 minutes to your webspace by executing `crontab -e` and adding ``` */15 * * * * scp <ABSOLUTE-PATH-TO>/data.csv <WEBHOSTING-USER>@<WEBHOSTING-HOST>:<PATH-WHERE-YOU-WANT-TO-STORE-YOUR-DATA>```
4. Now change inside app.R on Line 18 where it says <YOUR-URL-HERE> to the url to the data.csv on the server (eg. https://myserver.com/weather-data/data.csv).
5. Connect RStudio to your account at rshiny.com and publish the app.
6. (Optional) set up your (sub-) domain via frame forwarding to access the shiny server url.

### Final result
Once everything is set up, your dashboard should look something like this: 

![figure3](docs/img/weather-station.png "Preview")

