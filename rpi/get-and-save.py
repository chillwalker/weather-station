#!/usr/bin/env python
import sys
import csv
import datetime
import Adafruit_DHT

# Setup
sensor = Adafruit_DHT.DHT22
gpio = 4

# Try to grab a sensor reading.  Use the read_retry method which will retry up
# to 15 times to get a sensor reading (waiting 2 seconds between each retry).
humid, temp = Adafruit_DHT.read_retry(sensor, gpio)

# Prepare date and time variables for the output file.
date = datetime.datetime.now().strftime('%d.%m.%Y')
time = datetime.datetime.now().strftime('%H:%M:%S')

# Debugging
#print("temp: %s | humid: %s | date: %s | time: %s" % (temp, humid, date, time))

# Write to file
with open('data.csv', mode='a') as output_file:
  output_writer = csv.writer(output_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
  if humid is not None and temp is not None:
    output_writer.writerow([date, time, '{0:0.1f}'.format(temp), '{0:0.1f}'.format(humid)])
    #print('{0:0.1f}#{1:0.1f}'.format(temp, humid))
# Uncomment below, if you want to keep track of failed readings
#  else:
    #output_writer.writerow([date, time, 'NA', 'NA'])
    #print('Failed to get temps')
